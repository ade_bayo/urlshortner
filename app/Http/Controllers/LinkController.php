<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\DB;
use App\Link;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('homepage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $original_url = $request->all();

        $rules =  [
            'original_url' => 'required|url',
        ];

        $validator = Validator::make($request->all(), [
            'original_url' => 'required|url|max:255',
            ])->validate();
        
        $validation = Validator::make($original_url, $rules);

        if ($validation->fails()) {
            return Redirect::to('/')->withErrors($validation)->withInput();
        

        }else {
            $original_url = Link::where('original_url','=',Input::get('original_url'))->first();
            
            if($original_url) 
            {      
                // session('new_url', $original_url);
                $request->session()->flash('new_url', $original_url);
                return back();
                // return Redirect::to('/')->withInput()->with('original_url', $original_url->short_url);
                
            } else {

            do {
                
            $newURL = Str_random(6);
            
            } while(Link::where('short_url','=',$newURL)->count() > 0);

            $new_url = Link::create([
            'original_url' => Input::get('original_url'),
            'short_url' => $newURL
            
            ]);
            // session('new_url', $new_url);
            $request->session()->flash('new_url', $new_url);
            return back();

        //     $shortCode = 'http://127.0.0.1:8000/' . $newURL;
        //         // dd($shortCode);
     
            }
    }
}

    public function getURL (Request $request, $short_url)
    {
        $getURL = Link::where('short_url','=',$short_url)->first();
        if($getURL) 
        {
           return Redirect::to($getURL->original_url);

        } else {
            return ('KINDLY INSERT VALID LINK');
        }

      

        

    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
