<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    // protected $table = 'link';

    protected $fillable = [ 'id',
        'original_url', 'short_url', 'created_at', 'updated_at'
    ];
}
