@extends('layouts.app')
@section('content')
  
    <body>
        <header class="masthead text-white text-center">
            <div class="overlay"></div>
                <div class="container">
                  <div class="row">
                    <div class="col-xl-9 mx-auto">
                      <h2 class="mb-5">The worlds leading link management platform.</h2>
                    </div>
                    <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                        <div id="container">
                              {{--  Form::open(['action' => 'urlShortnerController@create', 'method' => 'POST', 'class'=>'container'])  --}}
                              <form class="container" method="POST" action="{{url('/')}}">
                                  {{ csrf_field() }}
                                  <input type="url" name="original_url" class="form-control form-control-lg" placeholder="Insert your URL here and press enter!" required>
                                  <br>
                                  
                                  <div class="col-md-3 col-lg-3 col-xl-7 mx-auto">
                                          <button type="submit"  class="btn btn-block btn-lg btn-primary">Shorten URL</button>
                                      </div>
                              </form>
                            @if (session('new_url'))
                                 <div class="row">
                                    <div class="col-sm-12">
                                        <h5>Original URL : </h5><a href="{{session('new_url')->original_url}}" target="_blank">{{session('new_url')->original_url}}</a> 
                                        <h5>Short URL : </h5><a href="{{url(session('new_url')->short_url)}}" target="_blank">{{url(session('new_url')->short_url)}}</a> 
                                        {{-- <b>Original URL:</b>{{session('new_url')->original_url}}
                                        <b>Shortened URL: </b>{{session('new_url')->short_url}} --}}
                                    </div>
                                </div>
                            @endif


                                @if(Session::has('errors'))
                                    <h3 class="error">{{$errors->first('original_url')}}</h3>
                                @endif   
          
      
      
                              @if (! Auth::guest())
                               {{-- @if(Auth::user()->id == $shortURL->user_id)   --}}
                                  <div>
                                      <a href="{{url('login')}}" class="btn btn-default secondary-bg btn-lg">Details</a>   
                                      {{--  {!!Form::open(['action'=>['ShortnerController@destroy', $shortURL->id], 'method'=> 'POST'])!!}
                                      {{Form::hidden('_method', 'DELETE')}}
                                      {{Form::submit('Delete', ['class'=> 'btn botton pull-right'])}}
                                      {!!Form::close()!!}   --}}
                                  </div>
                                  
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </header>
                    
                
                
           
    </body>
@endsection
